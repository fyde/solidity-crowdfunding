# README 
 
This is the final project from the B9lab Certified Online Ethereum Developer Course, built mainly with Solidity and Javascript.

These are the original instructions:

##FINAL PROJECT

You are going to build a decentralised crowdfunding application. Please create a truffle project for this.

###SUBMISSION

Please create a GitHub repository for your project and push your project folder there. Please create a README.md file with any notes you would like to add and submit the link to the repository when the submission opens on the learning platform. Remember to add the URL to your repository in the README file.

Submissions are open from December 6th 3PM GMT.

Please upload the README.md file of your project, containing the repository URL in the submission section below.

###CONTRACTS

Create two Solidity smart contracts named FundingHub and Project. Name the files FundingHub.sol and Project.sol.

####FUNDINGHUB

FundingHub is the registry of all Projects to be funded. FundingHub should have a constructor and the following functions:

createProject() - This function should allow a user to add a new project to the FundingHub. The function should deploy a new Project contract and keep track of its address. The createProject() function should accept all constructor values that the Project contract requires.

contribute() - This function allows users to contribute to a Project identified by its address. contribute calls the fund() function in the individual Project contract and passes on all value attached to the function call.

In Truffle, create a migration script that calls the createProject function after FundingHub has been deployed.

####PROJECT

Project is the contract that stores all the data of each project. Project should have a constructor and a struct to store the following information:

the address of the owner of the project
the amount to be raised (eg 100000 wei)
the deadline, i.e. the time until when the amount has to be raised
Please also implement the following functions:

fund() - This is the function called when the FundingHub receives a contribution. The function must keep track of the contributor and the individual amount contributed. If the contribution was sent after the deadline of the project passed, or the full amount has been reached, the function must return the value to the originator of the transaction and call one of two functions. If the full funding amount has been reached, the function must call payout. If the deadline has passed without the funding goal being reached, the function must call refund.

payout() - This is the function that sends all funds received in the contract to the owner of the project.

refund() - This function sends all individual contributions back to the respective contributor, or lets all contributors retrieve their contributions.

###INTERFACE

Create a simple web interface for the crowdfunding application. The interface must allow users to do the following:

browse active Projects
create their own Project
contribute to a Project

###TESTS

Create an automated test that covers the refund function in the Project contract using the truffle testing framework. You don't need to write tests for any other functionality.